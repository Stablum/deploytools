#!/usr/bin/env python3

import argparse
import yaml
import os
import jinja2
import shutil
import paramiko
import time
import colored
import sys
import getpass
import glob
import contextlib

args = None # global set by main()

@contextlib.contextmanager
def pushd(new_dir):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(previous_dir)

def sh(cmd):
    print('running cmd:',cmd)
    os.system(cmd)

def fg(*arg, **kwargs):
    assert 'color' in list(kwargs.keys())
    color = kwargs['color']
    assert type(color) is str

    print(colored.attr('reset')+colored.fg(color)+" ".join(arg),colored.attr('reset'),flush=True)

class Remote(object):
    def __init__(self, user, hostname, password, port=22):
        self.user = user
        self.hostname = hostname
        self.password = password

        self.client = paramiko.client.SSHClient()
        self.client.load_system_host_keys()
        fg("ssh connecting to "+self.user+"@"+self.hostname+":"+str(port),color='yellow')
        self.client.connect(
            hostname=self.hostname,
            username=self.user,
            port=port
        )
        self.channel = self.client.invoke_shell()
        self.channel.set_combine_stderr(True)

    @property
    def nohup_filename(self):
        if 'nohup_seq' not in dir(self):
            self.nohup_seq = 0
        self.nohup_seq += 1
        return "/tmp/nohup_{}.out".format(self.nohup_seq)

    def __call__(self,cmds):
        if type(cmds) is str:
            cmds = [cmds]

        if self.nohup:
            nohup_cmds = []
            for cmd in cmds:
                nohup_cmds.append("nohup "+cmd+" > "+self.nohup_filename)
            cmds = nohup_cmds
        cmds_str = ";".join(cmds)


        self.ssh(cmds_str)

    def ssh(self,cmd):
        print()
        fg('sending command on ssh:', cmd, color='blue')
        self.channel.sendall(cmd+'\n')
        if args.verbose:
            print('\n')

    def do_recv(self):
        b = self.channel.recv(1000)
        print(colored.attr('reset')+colored.fg('dark_gray'))
        sys.stdout.buffer.write(b)
        print(colored.attr('reset'))
        return b

    def sudo(self,cmds):
        if type(cmds) is str:
            cmds = [cmds]
        sudo_cmds = [
            "sudo "+(" " if self.password is None else "-S -k ")+(
                " nohup bash -c '"
                if args.nohup
                else ""
            )+curr+(
                "' > "+self.nohup_filename
                if args.nohup
                else ""
            )+(
                " &"
                if args.parallel
                else ""
            )
            for curr
            in cmds
        ]
        for cmd in sudo_cmds:
            self.ssh(cmd)
            if self.password is not None:
                self.channel.sendall(self.password+'\n')
                if args.sleep:
                    time.sleep(args.sleep)
            if args.verbose:
                self.do_recv()
            if args.sleep:
                time.sleep(args.sleep)

    def rsync(self,ldir,rdir, port, exclude=()):
        fg("rsync "+ldir+" "+rdir+" port:"+str(port),color='yellow')
        if ldir[-1] == '/':
            ldir = ldir[:-1]
        print(colored.fg('orange_4b'))
        exclude = ('.git',) + exclude
        exclude_arg = " ".join(['--exclude='+curr for curr in exclude])
        sh("rsync -avz -e \"ssh -p {} \" --progress {} {} {}@{}:{}".format(
            port,
            ldir,
            exclude_arg,
            self.user,
            self.hostname,
            rdir
        ))
        print(colored.attr('reset'))

    def sudo_mkdir(self,d):
        self.sudo("mkdir -v "+d)

    def ask_termination(self):
        self.ssh('python3 -c "print(\'TERM\'+\'INATED\')"')

    def wait_for_termination(self):
        while True:
            b = self.do_recv()
            if b'TERMINATED' in b:
                fg('got termination message',color='yellow')
                return

def render_template(filename, **kwargs):
    rendered_filename = os.path.join('rendered',filename)
    rendered_dirs = os.path.dirname(rendered_filename)
    os.makedirs(rendered_dirs,exist_ok=True)
    tpl_filename = os.path.join(
        os.path.dirname(
            os.path.abspath(__file__)
        ),
        os.path.join('templates',filename)
    )
    fg('tpl_filename',tpl_filename,color='orange_red_1')
    tpl = jinja2.Template(open(tpl_filename,'r').read())
    rendered = tpl.render(**kwargs)
    if args.verbose:
        fg(rendered,color='dark_olive_green_3a')
    rendered_file = open(rendered_filename,'w')
    rendered_file.write(rendered)
    rendered_file.flush()
    rendered_file.close()

def first_having(dicts, key):
    for d in dicts:
        if d is not None:
            if key in d.keys():
                return d[key]
    raise Exception('could not find '+key)

def ask_password():
    password = getpass.getpass('Please enter password used for sudo commands on remote server:')
    return password

def fix_srv_perm(remote, srv_dir):
    remote.sudo("chgrp -R www-data {}".format(srv_dir))
    remote.sudo("chmod g+w {}".format(srv_dir))

def main():
    global args
    parser = argparse.ArgumentParser(description='Deploy Tools')
    parser.add_argument("project_dir",nargs=1)
    parser.add_argument("app",nargs=1)
    parser.add_argument('server', nargs=1)
    parser.add_argument('-v','--verbose', action='store_true', help="verbose")
    parser.add_argument(
        '-s',
        '--sleep',
        type=int,
        help="sleep x seconds after running commands"
    )
    parser.add_argument(
        '-p',
        '--parallel',
        action='store_true',
        help="various commands are executed in parallel (& added in shell)"
    )
    parser.add_argument(
        '-n',
        '--nohup',
        action='store_true',
        help="use nohup"
    )


    args = parser.parse_args()
    project_dir = args.project_dir[0]
    project = list(filter(lambda x:x, project_dir.split('/')))[-1]
    print('args',args)
    print('project',project)
    app = args.app[0]
    server = args.server[0]

    yaml_filename = os.path.join(project_dir,'deploy.yaml')
    yaml_file = open(yaml_filename,'r')
    project_specs = yaml.load(yaml_file)
    app_specs = project_specs[app]
    srv_specs = app_specs[server]

    def look_for(key, default=None):
        # node upwards search
        try:
            return first_having([
                srv_specs,
                app_specs,
                project_specs
            ],key)
        except Exception as e:
            if default is None:
                raise e
            else:
                return default

    srv_dir = look_for('srv_dir')
    hostnames = look_for('hostname')
    hostname = hostnames.split(' ')[0]
    user = look_for('user')
    ssh_port = look_for('ssh_port',22)
    http_port = look_for('http_port',80)
    https_port = look_for('https_port',443)
    passwordless_sudo = look_for('passwordless_sudo',False)
    sock = os.path.join(srv_dir,project,'uwsgi',app+".sock")
    uwsgi_dir = os.path.join(srv_dir,project,'uwsgi')
    uwsgi_ini = os.path.join(uwsgi_dir,app+".ini")
    params = dict(
        project=project,
        project_dir=project_dir,
        app=app,
        srv_dir=srv_dir,
        hostname=hostname,
        hostnames=hostnames,
        sock=sock,
        user=user,
        uwsgi_ini=uwsgi_ini,
        http_port=http_port,
        https_port=https_port
    )

    unexpected_params = dict()
    for d in (srv_specs, app_specs, project_specs):
        for k,v in d.items():
            if type(v) not in (float, int,str, type(None)):
                # ignoring entire sections
                continue
            if k not in params.keys():
                # was not in the ones already cooked
                unexpected_params[k] = v
    params.update(unexpected_params)
    params['unexpected_params'] = unexpected_params # "reflection"
    print('os.getcwd 0',os.getcwd())
    with pushd('templates/'):
        print('os.getcwd 1',os.getcwd())
        templates = [f for f in glob.glob("**/*.*", recursive=True)]
    print('os.getcwd 2',os.getcwd())
    print("templates",templates)
    for template in templates:
        render_template(template, **params)

    tmp_dir="/tmp/"

    if passwordless_sudo:
        password = None
    else:
        password = ask_password()
    remote = Remote(user, hostname, password, port=ssh_port)
    remote.sudo_mkdir(tmp_dir)
    remote.sudo_mkdir(srv_dir)
    fix_srv_perm(remote,srv_dir)
    remote.sudo("chmod -R g+w {}".format(os.path.join(srv_dir,project)))
    remote.rsync(project_dir, srv_dir, ssh_port, exclude=("venv",))
    remote.rsync('rendered/', tmp_dir, ssh_port)
    remote.sudo_mkdir(uwsgi_dir)
    remote.sudo("chmod -R g+w {}".format(os.path.join(srv_dir,project)))
    install_commands = []
    for curr in templates:
        if curr.split('/')[-1] == 'install.sh':
            install_commands.append('bash -x {}/rendered/{}'.format(tmp_dir,curr))
    for cmd in install_commands:
        remote.sudo(cmd)
    remote.sudo("chgrp -R www-data {}".format(srv_dir))
    remote.sudo([
        "usermod -a -G www-data "+user,
        "chmod -R g+w {}".format(os.path.join(srv_dir,project)),
        ])
    fix_srv_perm(remote,srv_dir)
    remote.ask_termination()
    if args.sleep:
        time.sleep(args.sleep)
    remote.wait_for_termination()
    fg("all done.",color='blue')
if __name__ == "__main__":
    main()
