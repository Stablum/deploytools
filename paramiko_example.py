#!/usr/bin/env python3

import sys
import paramiko
import deploy

def main():
    hostname = sys.argv[1]
    username = sys.argv[2]

    password = deploy.ask_password()
    remote = deploy.Remote(username, hostname, password)
    remote.ssh('touch /tmp/paramiko_example1')
    remote.sudo('touch /tmp/paramiko_example2')
    remote.sudo('touch /tmp/paramiko_example3')
    import ipdb; ipdb.set_trace()

if __name__=="__main__":
    main()