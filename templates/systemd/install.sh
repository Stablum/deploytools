#!/usr/bin/env bash


SERVICENAME={{project}}_{{app}}
D=$(dirname ${BASH_SOURCE[0]})
systemctl stop $SERVICENAME.service
SERVICEFILE=/lib/systemd/system/$SERVICENAME.service
RUNFILE={{srv_dir}}/{{project}}/run_{{app}}.sh
cp -vf $D/template.service $SERVICEFILE
cp -vf $D/run.sh $RUNFILE
chmod a+x $SERVICEFILE $RUNFILE
systemctl daemon-reload
systemd-analyze verify $SERVICENAME.service
systemctl enable $SERVICENAME.service
systemctl start $SERVICENAME.service
#systemctl status sito_app.service
