#!/usr/bin/env bash

FILENAME={{project}}_{{app}}.conf

D=$(dirname ${BASH_SOURCE[0]})
cp -vf $D/template.conf /etc/nginx/conf.d/$FILENAME
cp -vf $D/template.conf /etc/nginx/sites-available/$FILENAME
ln -s /etc/nginx/sites-available/$FILENAME /etc/nginx/sites-enabled/$FILENAME
cp -v {{srv_dir}}/{{project}}/ssl/{{project}}_{{app}}.crt /etc/ssl/
cp -v {{srv_dir}}/{{project}}/ssl/{{project}}_{{app}}.privatekey /etc/ssl/private/
systemctl stop nginx
systemctl start nginx
