#!/usr/bin/env bash

D=$(dirname ${BASH_SOURCE[0]})
cp -vf $D/generated.json {{srv_dir}}/{{project}}/config/{{app}}_generated.json
