#!/usr/bin/env bash

mv {{srv_dir}}/{{project}}/venv /tmp/{{project}}_venv_old_$(date +"%Y%m%d%H%M%S")

python3 -m venv {{srv_dir}}/{{project}}/venv --copies --clear
. {{srv_dir}}/{{project}}/venv/bin/activate

ACTIVATE={{srv_dir}}/{{project}}/venv/bin/activate
echo "ACTIVATE:$ACTIVATE"
. $ACTIVATE
for CURR in uwsgi jinja2 flask pyyaml colored sqlalchemy flask_sqlalchemy werkzeug \
    psycopg2 flask_jsontools flask_restful flask_security flask_httpauth flask_session flask-login; do
    pip3 install --upgrade $CURR
done
