#!/usr/bin/env bash

D=$(dirname ${BASH_SOURCE[0]})
mkdir {{srv_dir}}/{{project}}/uwsgi
cp -vf $D/template.ini {{uwsgi_ini}}
